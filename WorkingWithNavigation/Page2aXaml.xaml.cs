﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
namespace WorkingWithNavigation
{
	public partial class Page2aXaml : ContentPage
    {
        public static ObservableCollection<string> items { get; set; }
        public Page2aXaml ()
        {
            items = new ObservableCollection<string>()
            { "Police Control Room","Kochi Station", "Nedumbassery Station", "Aluva Station", "Angamaly Station", "Chalakudy" };
            InitializeComponent ();
		}

		async void OnNextPageButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new Page4 ());
		}

		async void OnPreviousPageButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PopAsync ();
		}
	}
}
