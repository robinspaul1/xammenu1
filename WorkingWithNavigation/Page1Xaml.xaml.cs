﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkingWithNavigation
{
    public class Employee
    {
        public string DisplayName { get; set; }
    }
    public partial class Page1Xaml : ContentPage
	{

        ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
        public static ObservableCollection<string> items { get; set; }
        public Page1Xaml ()
		{
            employees.Add(new Employee { DisplayName = "Rob Finnerty" });
            employees.Add(new Employee { DisplayName = "Bill Wrestler" });
            employees.Add(new Employee { DisplayName = "Dr. Geri-Beth Hooper" });
            employees.Add(new Employee { DisplayName = "Dr. Keith Joyce-Purdy" });
            employees.Add(new Employee { DisplayName = "Sheri Spruce" });
            employees.Add(new Employee { DisplayName = "Burt Indybrick" });
            items = new ObservableCollection<string>()
            {"Police", "Hospital", "Fire", "Childcare", "Charity" };
            //EmployeeView.ItemsSource = employees;
           // EmployeeView.ItemsSource = employees;
            InitializeComponent();
          
		}

		async void OnNextPageButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new Page2Xaml ());
		}



        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
         //   DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;


            await Navigation.PushAsync(new Page2Xaml());

        }

        void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            //put your refreshing logic here
            var itemList = items.Reverse().ToList();
            items.Clear();
            foreach (var s in itemList)
            {
                items.Add(s);
            }
            //make sure to end the refresh state
            list.IsRefreshing = false;
        }

        async void OnTap(object sender, ItemTappedEventArgs e)
        {
            // DisplayAlert("Item Tapped", e.Item.ToString(), "Ok");
            await Navigation.PushAsync(new Page2Xaml());
        }

        void OnMore(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            DisplayAlert("More Context Action", item.CommandParameter + " more context action", "OK");
        }

        void OnDelete(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            items.Remove(item.CommandParameter.ToString());
        }


    }
}

