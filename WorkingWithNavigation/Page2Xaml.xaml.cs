﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkingWithNavigation
{
	public partial class Page2Xaml : ContentPage
    {
        public static ObservableCollection<string> items { get; set; }
        public Page2Xaml ()
        {
            items = new ObservableCollection<string>()
            { "Police Control Room","Kochi Station", "Nedumbassery Station", "Aluva Station", "Angamaly Station", "Chalakudy" };
            InitializeComponent ();
		}

		async void OnNextPageButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new Page4());
		}

		async void OnPreviousPageButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PopAsync ();
		}


        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            //   DisplayAlert("Item Selected", e.SelectedItem.ToString(), "Ok");
            //comment out if you want to keep selections
            ListView lst = (ListView)sender;
            lst.SelectedItem = null;


            await Navigation.PushAsync(new Page4());

        }

        void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            //put your refreshing logic here
            var itemList = items.Reverse().ToList();
            items.Clear();
            foreach (var s in itemList)
            {
                items.Add(s);
            }
            //make sure to end the refresh state
            list.IsRefreshing = false;
        }

        async void OnTap(object sender, ItemTappedEventArgs e)
        {
            // DisplayAlert("Item Tapped", e.Item.ToString(), "Ok");
            await Navigation.PushAsync(new Page4());
        }

        void OnMore(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            DisplayAlert("More Context Action", item.CommandParameter + " more context action", "OK");
        }

        void OnDelete(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            items.Remove(item.CommandParameter.ToString());
        }


    }
}

